use std::{
	fs,
	io::{Read, Write},
	net::TcpStream,
	sync::{Arc, Mutex},
};

use rocket::{
	response::{content::Html, Redirect},
	State,
};
use rscam::{Camera, Config};
use ssh2::Session;

#[macro_use]
extern crate rocket;

mod config;

struct Counter {
	pub count: usize,
}
impl Counter {
	fn increment(&mut self) {
		self.count += 1;
	}
	fn decrement(&mut self) {
		if self.count > 0 {
			self.count -= 1;
		}
	}
	fn set(&mut self, tgt: usize) {
		self.count = tgt;
	}
}

#[tokio::main]
async fn main() {
	let session_ptr = {
		let tcp = TcpStream::connect(config::SSH_HOST).unwrap();
		let mut session = Session::new().unwrap();
		session.set_tcp_stream(tcp);
		session.handshake().unwrap();
		session.userauth_agent(config::SSH_USER).unwrap();
		Arc::new(Mutex::new(session))
	};
	let camera_ptr = {
		let mut camera = Camera::new(config::VIDEO_DEVICE).unwrap();
		camera
			.start(&Config {
				interval: (1, 30),
				resolution: (1280, 720),
				format: b"MJPG",
				..Default::default()
			})
			.unwrap();
		Arc::new(Mutex::new(camera))
	};
	let counter_ptr = {
		let counter = 0;
		Arc::new(Mutex::new(Counter { count: counter }))
	};
	rocket::ignite()
		.manage(session_ptr)
		.manage(camera_ptr)
		.manage(counter_ptr)
		.mount("/", routes![status, take, untake])
		.launch()
		.await
		.unwrap();
}

#[get("/")]
fn status(counter: State<Arc<Mutex<Counter>>>) -> Html<String> {
	Html(format!(
		"{} pictures taken,<br /><a href=\"/take\">take one more</a><br /><a href=\"/untake\">whoops</a>",
		counter.lock().unwrap().count
	))
}
#[get("/take")]
fn take(
	counter: State<Arc<Mutex<Counter>>>,
	camera: State<Arc<Mutex<Camera>>>,
	session: State<Arc<Mutex<Session>>>,
) -> Redirect {
	let counter_ = counter.try_lock();
	match counter_ {
		Ok(mut counter) => {
			let session = session.lock().unwrap();
			let mut channel = session.channel_session().unwrap();
			channel
				.exec(&format!(
					"SHOT_COUNTER={} {}",
					counter.count,
					config::SSH_COMMAND
				))
				.unwrap();
			let mut s = String::new();
			channel.read_to_string(&mut s).unwrap();
			println!("{}", s);
			channel.wait_close().unwrap();
			let camera = camera.lock().unwrap();
			let frame = camera.capture().unwrap();
			let mut file =
				fs::File::create(&format!("{}/{}.jpg", config::OUTPUT_PATH, counter.count))
					.unwrap();
			file.write_all(&frame[..]).unwrap();
			counter.increment();
			Redirect::to("/")
		}
		Err(_) => {
			eprintln!("Requests are coming in too fast, or one panicked!");
			Redirect::to("/")
		}
	}
}
#[get("/untake")]
fn untake(counter: State<Arc<Mutex<Counter>>>, session: State<Arc<Mutex<Session>>>) -> Redirect {
	let session = session.lock().unwrap();
	let mut channel = session.channel_session().unwrap();
	let mut counter = counter.lock().unwrap();
	channel
		.exec(&format!(
			"SHOT_COUNTER={} {}",
			counter.count,
			config::SSH_BACK_COMMAND
		))
		.unwrap();
	let mut s = String::new();
	channel.read_to_string(&mut s).unwrap();
	println!("{}", s);
	channel.wait_close().unwrap();
	counter.decrement();
	Redirect::to("/")
}
#[get("/set/<tgt>")]
fn set(counter: State<Arc<Mutex<Counter>>>, tgt: usize) -> Redirect {
	counter.lock().unwrap().set(tgt);
	Redirect::to("/")
}
