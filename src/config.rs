pub const SSH_HOST: &'static str = "10.0.128.232:22";
pub const SSH_USER: &'static str = "willow";
pub const SSH_COMMAND: &'static str = "DISPLAY=:1 bash -c 'xdotool key Alt+i key f & sleep 1s && xdotool key Alt+i'";
pub const SSH_BACK_COMMAND: &'static str = "DISPLAY=:1 xdotool key Shift+r";
pub const VIDEO_DEVICE: &'static str = "/dev/video0";
pub const OUTPUT_PATH: &'static str = "/home/pi/Pictures";
